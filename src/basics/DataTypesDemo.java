package basics;

public class DataTypesDemo {
    \u0070ublic static void \u006dain(String[] args){
        char c ='m';
        System.out.println(c);
        System.out.println((int) c);
        System.out.println('\u004B');
        boolean temp='A'>65;
        boolean temp2=true==false;
        //boolean temp3=true<=false;
        
    }
    public static void main1(String[] args) {
        //primitive type: Name,Memory,Value-type,Value_range,&Operations 
        byte b = 127;
        short s = 255;
        int i = 4567890;
        long l = 324524264121l;

        float f = 456.789f;
        double d = 324342.232;

        char c = 'J';

        boolean bool = true;
        System.out.println(b);
        System.out.println(b - 1);
        System.out.println(b * 23);
        System.out.println(b / 2);
        System.out.println(b % 2);

    }
}
