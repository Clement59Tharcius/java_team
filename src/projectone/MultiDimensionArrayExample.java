/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectone;

/**
 *
 * @author ClementTharcius
 */
public class MultiDimensionArrayExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        // TODO code application logic here
        int[] oneDimensionalArray = {12, 34, 122};
        System.out.println(oneDimensionalArray[1]);

        int[][] multiDimensionalArray = {{12, 32, 355}, {121, 43543, 24332, 123}, {12, 21, 12}};
        System.out.println(multiDimensionalArray[1][3]);
//        System.out.println(multiDimensionalArray[2][1]);

        double[][] doublearray = new double[4][2];

        doublearray[3][1] = 2.9;
        System.out.println(doublearray[3][1]);

        for (int array = 0; array < multiDimensionalArray.length; array++) {
            for (int item = 0; item < multiDimensionalArray[array].length; item++) {
                System.out.print(multiDimensionalArray[array][item] + "\t");
            }
           System.out.println();
        }
    }
}
