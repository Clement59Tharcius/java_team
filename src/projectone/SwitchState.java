/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectone;

import java.util.Scanner;

/**
 *
 * @author ClementTharcius
 */
public class SwitchState {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        // TODO code application logic here
        Scanner input = new Scanner(System.in);
        System.out.println("Enter your instructions: ");
        String text = input.nextLine();
        
        switch(text){
            case "run":
                System.out.println("Program is running");
                break;
            case "stop":
                System.out.println("Stopped");
                break;
            default:
                System.out.println("Instructions not recognized");
        }
    }
}
