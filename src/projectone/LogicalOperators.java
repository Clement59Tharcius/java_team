/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projectone;

/**
 *
 * @author ClementTharcius
 */
public class LogicalOperators {

    /**
     * @param args the command line arguments
     */
    public static void main1(String args[]) {
        // TODO code application logic here
        int a = 10 + 20;
        boolean b = 20 > 20;//assing and logic
        boolean m = false || false;
        boolean m1 = false || true;
        boolean m2 = true || false;
        boolean m4 = true || true;

        System.out.println(m);
        System.out.println(m1);
        System.out.println(m2);
        System.out.println(m4);

    }

    public static void main(String args[]) {
        boolean m1 = (true && (10 > (12 - 1)));
        System.err.println(m1);
        boolean m2 = 10 < 5 || 3 > 4;
        boolean m3 = !(10 < 14); //here Not operator is a priority
    }
}
